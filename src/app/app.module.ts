import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {DashboardComponent} from './shared/module/dashboard/dashboard.component';
import {AppRoutingModule} from './app-routing.module';
import {FormsModule} from '@angular/forms';
import {ButtonModule} from 'primeng/button';
import {DialogModule} from 'primeng/dialog';
import {TableModule} from 'primeng/table';
import {HttpClientModule} from '@angular/common/http';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {AccordionModule, ScrollPanelModule} from 'primeng/primeng';
import {GameListComponent} from './shared/module/game-list/game-list.component';
import {GameLogsArchiveComponent} from './shared/module/game-logs-archive/game-logs-archive.component';
import {PlayGameComponent} from './shared/module/game-visualisation/play-game/play-game.component';
import {AdminPanelModule} from './shared/module/admin-panel/admin-panel.module';
import {DropdownModule} from 'primeng/dropdown';
import { StartGameComponent } from './shared/module/start-game/start-game.component';
import {ProgressSpinnerModule} from 'primeng/progressspinner';
import {ToastModule} from 'primeng/toast';
import {PlaybackComponent} from './shared/module/game-visualisation/playback/playback.component';
import { CreditsComponent } from './shared/module/credits/credits.component';

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    GameListComponent,
    GameLogsArchiveComponent,
    PlaybackComponent,
    PlayGameComponent,
    StartGameComponent,
    CreditsComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    TableModule,
    ToastModule,
    ButtonModule,
    FormsModule,
    ScrollPanelModule,
    AppRoutingModule,
    DialogModule,
    AccordionModule,
    AdminPanelModule,
    DropdownModule,
    ProgressSpinnerModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
