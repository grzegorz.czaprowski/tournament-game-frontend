import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {GameState} from '../../model/GameState';
import {environment} from '../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class MapService {

  constructor(private http: HttpClient) { }

  getTiledMap(): Observable<number[][]> {
    return this.http.get<number[][]>(
      `${environment.url}/getMap`
    );
  }

  getLiveGameState(gameId: number): Observable<GameState> {
    return this.http.get<GameState>(
      `${environment.url}/getCurrentGameState/${gameId}`
    );
  }

  getGameStates(gameId: number): Observable<GameState[]> {
    return this.http.get<GameState[]>(
      `${environment.url}/getGameStates/${gameId}`
    );
  }

  public readCell(value: number): string {
    switch (value) {
      case 1:
        return 'td-grass';
      case 2:
        return 'td-water';
      case 3:
        return 'td-tree';
      case 4:
        return 'td-base';
      case 5:
        return 'td-mine';
      case 8:
        return 'td-worker1';
      case 9:
        return 'td-warrior1';
      case 10:
        return 'td-archer1';
      case 11:
        return 'td-horse1';
      case 12:
        return 'td-worker2';
      case 13:
        return 'td-warrior2';
      case 14:
        return 'td-archer2';
      case 15:
        return 'td-horse2';
      case 18:
        return 'td-worker1-e';
      case 19:
        return 'td-warrior1-e';
      case 20:
        return 'td-archer1-e';
      case 21:
        return 'td-horse1-e';
      case 22:
        return 'td-worker2-e';
      case 23:
        return 'td-warrior2-e';
      case 24:
        return 'td-archer2-e';
      case 25:
        return 'td-horse2-e';
      case null:
        return '  ';
      default:
        return 'td-earth';
    }
  }

  updateUnitsOnMap(unitsMap: number[][], gameState: GameState) {
    gameState.player1.units.forEach(unit => {
      switch (unit.name) {
        case 'ARCHER':
          if (unit.entrench) {
            unitsMap[(unitsMap.length - 1) - unit.coordinates.y][unit.coordinates.x] = 20;
          } else {
            unitsMap[(unitsMap.length - 1) - unit.coordinates.y][unit.coordinates.x] = 10;
          }
          break;
        case 'HORSE':
          if (unit.entrench) {
            unitsMap[(unitsMap.length - 1) - unit.coordinates.y][unit.coordinates.x] = 21;
          } else {
            unitsMap[(unitsMap.length - 1) - unit.coordinates.y][unit.coordinates.x] = 11;
          }
          break;
        case 'WARRIOR':
          if (unit.entrench) {
            unitsMap[(unitsMap.length - 1) - unit.coordinates.y][unit.coordinates.x] = 19;
          } else {
            unitsMap[(unitsMap.length - 1) - unit.coordinates.y][unit.coordinates.x] = 9;
          }
          break;
        case 'WORKER':
          if (unit.entrench) {
            unitsMap[(unitsMap.length - 1) - unit.coordinates.y][unit.coordinates.x] = 18;
          } else {
            unitsMap[(unitsMap.length - 1) - unit.coordinates.y][unit.coordinates.x] = 8;
          }
          break;
        default:
          unitsMap[(unitsMap.length - 1) - unit.coordinates.y][unit.coordinates.x] = 9;
          break;
      }
    });
    gameState.player2.units.forEach(unit => {
      switch (unit.name) {
        case 'ARCHER':
          if (unit.entrench) {
            unitsMap[(unitsMap.length - 1) - unit.coordinates.y][unit.coordinates.x] = 24;
          } else {
            unitsMap[(unitsMap.length - 1) - unit.coordinates.y][unit.coordinates.x] = 14;
          }
          break;
        case 'HORSE':
          if (unit.entrench) {
            unitsMap[(unitsMap.length - 1) - unit.coordinates.y][unit.coordinates.x] = 25;
          } else {
            unitsMap[(unitsMap.length - 1) - unit.coordinates.y][unit.coordinates.x] = 15;
          }
          break;
        case 'WARRIOR':
          if (unit.entrench) {
            unitsMap[(unitsMap.length - 1) - unit.coordinates.y][unit.coordinates.x] = 23;
          } else {
            unitsMap[(unitsMap.length - 1) - unit.coordinates.y][unit.coordinates.x] = 13;
          }
          break;
        case 'WORKER':
          if (unit.entrench) {
            unitsMap[(unitsMap.length - 1) - unit.coordinates.y][unit.coordinates.x] = 22;
          } else {
            unitsMap[(unitsMap.length - 1) - unit.coordinates.y][unit.coordinates.x] = 12;
          }
          break;
        default:
          unitsMap[(unitsMap.length - 1) - unit.coordinates.y][unit.coordinates.x] = 13;
          break;
      }
    });
  }
}
