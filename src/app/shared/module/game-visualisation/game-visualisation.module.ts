import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ScrollPanelModule} from 'primeng/primeng';
import {PlayGameComponent} from './play-game/play-game.component';

@NgModule({
  imports: [
    CommonModule,
    ScrollPanelModule,
    PlayGameComponent
  ],
  declarations: [
  ]
})
export class GameVisualisationModule {
}
