import {Component, OnDestroy, OnInit} from '@angular/core';
import {interval} from 'rxjs';
import {MapService} from '../map.service';
import {MessageService} from 'primeng/api';
import {GameState} from '../../../model/GameState';
import {ActivatedRoute} from '@angular/router';
import {PlayGameService} from '../../../service/play-game.service';

@Component({
  selector: 'app-play-game',
  templateUrl: './play-game.component.html',
  styleUrls: ['../map-styles.css', './play-game.component.css'],
  providers: [MessageService]
})
export class PlayGameComponent implements OnInit, OnDestroy {

  unitsMap: number[][];
  tiledMap: number[][];
  gameId: number;
  step = 0;
  currentTurn = 0;
  source = interval(300);
  winnerId: number;
  player1Id: number;

  display: boolean;
  subscribeClosed: boolean;
  playerWinnerShieldClass;

  constructor(public mapService: MapService,
              private messageService: MessageService,
              private route: ActivatedRoute,
              private playGameService: PlayGameService) {
  }

  ngOnInit() {
    this.loadGameIdFromUrl();
    this.fillUnitsMap();
    this.getTiledMap();
    this.source.subscribe(() => {
      this.getResultGame();
      this.getGameState();
    });
  }

  ngOnDestroy() {
    this.subscribeClosed = this.source.subscribe().closed;
  }

  public loadGameIdFromUrl() {
    this.route.params.subscribe(params => {
      this.gameId = params['gameId'];
    });
  }

  public getResultGame() {
    this.playGameService.getResultGame(this.gameId).subscribe(
      response => {
        if (response) {
          this.winnerId = response.winnerId;
          this.getPlayerNumber();
          this.display = true;
        }
      }
    );
  }

  showInfo(action: string, details: string) {
    this.messageService.add({severity: 'info', summary: action, detail: details});
  }

  public getTiledMap() {
    this.mapService.getTiledMap().subscribe(response => {
      this.tiledMap = response;
    });
  }

  public fillUnitsMap() {
    this.mapService.getTiledMap().subscribe(response => {
      this.unitsMap = response;
      for (let i = 0; i < response.length; i++) {
        for (let y = 0; y < response[i].length; y++) {
          this.unitsMap[i][y] = null;
        }
      }
    });
  }

  clearUnitOnMap() {
    for (let i = 0; i < this.unitsMap.length; i++) {
      this.unitsMap[i].fill(null);
    }
  }

  public getGameState() {
    this.mapService.getLiveGameState(this.gameId)
      .subscribe(response => {
        if (response && this.step < response.currentStep) {
          this.step = response.currentStep;
          if (this.step % 2 === 0) {
            this.currentTurn = this.currentTurn + 1;
          }
          this.player1Id = response.player1.id;
          this.clearUnitOnMap();
          this.mapService.updateUnitsOnMap(this.unitsMap, response);
          this.showActions(response);
          this.showInfo('Current players state', this.gameStateToLog(response));
        }
      });
  }

  public showActions(gameState: GameState) {
    if (gameState && gameState.player1.active) {
      gameState.lastLogs.forEach( log => {
        this.showInfo('Player 2 action', '🔰 ' + log);
      });
    } else if (gameState) {
      gameState.lastLogs.forEach(log => {
        this.showInfo('Player 1 action', '🔰 ' + log);
      });
    }
  }

  public gameStateToLog(gameState: GameState): string {
    if (gameState.player1.active) {
      return 'Player 1: ' + gameState.player1.gold + ' gold, ' + gameState.player1.units.length + ' units.';
    } else {
      return 'Player 2: ' + gameState.player2.gold + ' gold, ' + gameState.player2.units.length + ' units.';
    }
  }

  getPlayerNumber() {
    if (this.winnerId === this.player1Id) {
      this.playerWinnerShieldClass = 'shield-img-p1';
    } else {
      this.playerWinnerShieldClass = 'shield-img-p2';
    }
  }

  getPlayerTurn(): string {
    if (this.step % 2 === 0) {
      return 'player 1';
    } else {
      return 'player 2';
    }
  }

}
