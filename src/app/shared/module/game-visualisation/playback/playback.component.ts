import {Component, OnInit} from '@angular/core';
import {GameState} from '../../../model/GameState';
import {MapService} from '../map.service';
import {ActivatedRoute} from '@angular/router';
import {PlayGameService} from '../../../service/play-game.service';

@Component({
  selector: 'app-playback',
  templateUrl: './playback.component.html',
  styleUrls: ['../map-styles.css', './playback.component.css']
})
export class PlaybackComponent implements OnInit {

  gameId: number;
  gameStates: GameState[] = [];
  turn = 0;
  result: string;
  winnerId: number;
  unitsMap: number[][] = [];
  tiledMap: number[][] = [];
  isAutoPlay: boolean;
  turnDisplay: number;
  display: boolean;
  btnNextDisabled: boolean;
  btnPrevDisabled: boolean;
  completeLoad = false;
  statisticPlayer1StyleName = 'active-player';
  statisticPlayer2StyleName = 'inactive-player';
  playerWinnerShieldClass: string;
  workersP1 = 0;
  warriorsP1 = 0;
  archersP1 = 0;
  horsesP1 = 0;
  goldP1 = 0;
  workersP2 = 0;
  warriorsP2 = 0;
  archersP2 = 0;
  horsesP2 = 0;
  goldP2 = 0;
  playerActive = 'Game Start';

  constructor(public mapService: MapService,
              private route: ActivatedRoute,
              private playGameService: PlayGameService) {
  }

  ngOnInit() {
    this.loadGameIdFromUrl();
    this.getGameStates();
    this.fillUnitsMap();
    this.getTiledMap();
    this.btnPrevDisabled = true;
    this.turnDisplay = 1;
    this.getResultGame();
  }

  public loadGameIdFromUrl() {
    this.route.params.subscribe(params => {
      this.gameId = params['gameId'];
    });
  }

  public getTiledMap() {
    this.mapService.getTiledMap().subscribe(response => {
      this.tiledMap = response;
    });
  }

  public fillUnitsMap() {
    this.mapService.getTiledMap().subscribe(response => {
      this.unitsMap = response;
      for (let i = 0; i < response.length; i++) {
        for (let y = 0; y < response[i].length; y++) {
          this.unitsMap[i][y] = null;
        }
      }
    });
  }

  public getGameStates() {
    this.mapService.getGameStates(this.gameId)
      .subscribe(response => {
        this.gameStates = response;
        this.completeLoad = true;
      });
  }

  public setSelectedGameState() {
    for (let i = 0; i < this.unitsMap.length; i++) {
      this.unitsMap[i].fill(null);
    }
    this.mapService.updateUnitsOnMap(this.unitsMap, this.gameStates[this.turn]);
    this.highlightPlayer();
    this.setStatistics();
  }

  public highlightPlayer() {
    if (this.gameStates[this.turn].player1.active) {
      this.statisticPlayer1StyleName = 'active-player';
      this.statisticPlayer2StyleName = 'inactive-player';
    } else {
      this.statisticPlayer1StyleName = 'inactive-player';
      this.statisticPlayer2StyleName = 'active-player';
    }
  }

  public setStatistics() {
    this.goldP1 = this.gameStates[this.turn].player1.gold;
    this.workersP1 = this.getValueOfTypeUnits1('WORKER');
    this.warriorsP1 = this.getValueOfTypeUnits1('WARRIOR');
    this.archersP1 = this.getValueOfTypeUnits1('ARCHER');
    this.horsesP1 = this.getValueOfTypeUnits1('HORSE');
    this.goldP2 = this.gameStates[this.turn].player2.gold;
    this.workersP2 = this.getValueOfTypeUnits2('WORKER');
    this.warriorsP2 = this.getValueOfTypeUnits2('WARRIOR');
    this.archersP2 = this.getValueOfTypeUnits2('ARCHER');
    this.horsesP2 = this.getValueOfTypeUnits2('HORSE');
  }

  public getValueOfTypeUnits1(type: string): number {
    let count = 0;
    this.gameStates[this.turn].player1.units.forEach( unit => {
      if (unit.name === type) {
        count++;
      }
    });
    return count;
  }
  public getValueOfTypeUnits2(type: string): number {
    let count = 0;
    this.gameStates[this.turn].player2.units.forEach( unit => {
      if (unit.name === type) {
        count++;
      }
    });
    return count;
  }

  public gameStateToLog(gameState: GameState): string {
    if (gameState.player1.active) {
      this.result = ('Player 1: ' + gameState.player1.gold + ' gold, ');
      this.result += (gameState.player1.units.length + ' units.');
    } else {
      this.result = ('Player 2: ' + gameState.player2.gold + ' gold, ');
      this.result += (gameState.player2.units.length + ' units.');
    }
    return this.result;
  }

  previewTurn() {
    if (this.turn === 1) {
      this.btnPrevDisabled = true;
    }
    this.btnNextDisabled = false;
    if (this.turn !== 0) {
      this.turn = this.turn - 1;
      this.turnDisplay = this.gameStates[this.turn].currentTurn + 1;
      this.setSelectedGameState();
      window.location.hash = `turn${this.gameStates[this.turn].currentTurn}`;
    }

    if (this.gameStates[this.turn] && this.gameStates[this.turn].player1.active === true) {
      this.playerActive = 'player 1';
    } else {
      this.playerActive = 'player 2';
    }
  }

  getResultGame() {
    this.playGameService.getResultGame(this.gameId).subscribe( res => this.winnerId = res.winnerId );
  }

  nextTurn() {
    if (this.turn === this.gameStates.length - 2) {
      this.btnNextDisabled = true;
      this.getResultGame();
      this.setPlayerWinnerShieldClass();
      this.display = true;
    }
    this.btnPrevDisabled = false;

    if (this.turn < this.gameStates.length - 1) {
      this.turn = this.turn + 1;
      this.turnDisplay = this.gameStates[this.turn].currentTurn + 1;
      this.setSelectedGameState();
      window.location.hash = `turn${this.gameStates[this.turn].currentTurn}`;
    }

    if (this.gameStates[this.turn] && this.gameStates[this.turn].player1.active === true) {
      this.playerActive = 'player 1';
    } else {
      this.playerActive = 'player 2';
    }
  }

  goTime() {
    setTimeout(() => {
      this.nextTurn();
      if (this.isAutoPlay) {
        this.autoPlay();
      }
    }, 100);
  }

  pause() {
    this.isAutoPlay = false;
  }

  autoPlay() {
    this.isAutoPlay = true;
    if (this.turn < this.gameStates.length - 1) {
      this.goTime();
    }
  }

  confirmDialog() {
    this.display = false;
  }

  setPlayerWinnerShieldClass() {
    if (this.winnerId === this.gameStates[0].player1.id) {
      this.playerWinnerShieldClass = 'shield-img-p1';
    } else {
      this.playerWinnerShieldClass = 'shield-img-p2';
    }
  }

}
