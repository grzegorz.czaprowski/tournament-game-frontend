import { Component, OnInit } from '@angular/core';
import {GameData} from '../../model/GameData';
import {StartGameService} from '../../service/start-game.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-start-game',
  templateUrl: './start-game.component.html',
  styleUrls: ['./start-game.component.css']
})
export class StartGameComponent implements OnInit {

  gameData = new GameData();
  countdown = 5;
  player1Url: string;
  player2Url: string;
  visible: boolean;

  constructor(private startGameService: StartGameService, private router: Router) { }

  ngOnInit() {
  }

  public delayNavigate() {
    setTimeout(() => {
      this.countdown = this.countdown - 1;
      if (this.countdown === 0) {
        this.router.navigateByUrl(`/play-game/${this.gameData.gameID}`);
      } else {
        this.delayNavigate();
      }
    }, 1000);
  }

  public startGame() {
    this.visible = !this.visible;
    this.gameData.gameID = Math.floor(Math.random() * 999999) + 100000;
    this.gameData.player1Url = this.player1Url;
    this.gameData.player2Url = this.player2Url;
    this.gameData.player1ID = Math.floor(Math.random() * 555555) + 100000;
    this.gameData.player2ID = Math.floor(Math.random() * 999999) + 555555;
    this.gameData.responseUrl = `http://localhost:4200/play-game/${this.gameData.gameID}`;
    this.startGameService.newGame(this.gameData).subscribe();
    this.delayNavigate();
  }

}
