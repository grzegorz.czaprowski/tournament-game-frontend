import {Component, OnInit} from '@angular/core';
import {GameLogsArchiveService} from '../../service/game-logs-archive.service';
import {ArchiveGame} from '../../model/ArchiveGame';

@Component({
  selector: 'app-game-logs-archive',
  templateUrl: './game-logs-archive.component.html',
  styleUrls: ['./game-logs-archive.component.css']
})
export class GameLogsArchiveComponent implements OnInit {
  gamesList: ArchiveGame[];

  constructor(private gameLogsArchiveService: GameLogsArchiveService) {
  }

  ngOnInit() {
    this.getArchiveGames();
  }

  public getArchiveGames() {
    this.gameLogsArchiveService.getArchiveGamesList()
      .subscribe(response => {
        this.gamesList = response;
      });
  }
}
