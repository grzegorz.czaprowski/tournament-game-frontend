import { Component, OnInit } from '@angular/core';
import {GameListService} from '../../service/game-list.service';

@Component({
  selector: 'app-game-list',
  templateUrl: './game-list.component.html',
  styleUrls: ['./game-list.component.css']
})
export class GameListComponent implements OnInit {

  activeGames: number[] = [];

  constructor(private gameListService: GameListService) { }

  ngOnInit() {
    this.setActiveGames();
  }

  setActiveGames() {
    this.gameListService.getActiveGames().subscribe(response => this.activeGames = response);
  }
}
