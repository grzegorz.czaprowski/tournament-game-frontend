import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {AdminLoginData} from './model/AdminLoginData';
import {environment} from '../../../../environments/environment';
import {Observable} from 'rxjs';
import {Unit} from './model/Unit';

@Injectable({
  providedIn: 'root'
})
export class AdminPanelService {

  constructor(private http: HttpClient) {
  }

  validateAdmin(adminLoginData: AdminLoginData): Observable<AdminLoginData> {
    return this.http.post<AdminLoginData>(`${environment.url}/admin/sign-in`, adminLoginData);
  }

  changePassword() {

  }

  getUnitParams(): Observable<Unit[]> {
    return this.http.get<Unit[]>(`${environment.url}/admin/get-unit-params`);
  }

  getMiningParams() {
    return this.http.get<number[]>(`${environment.url}/admin/get-mining-params`);
  }

  editUnitParams(units: Unit[]) {
    return this.http.post<Unit[]>(`${environment.url}/admin/edit-unit-params`, units);
  }

  editMiningParams(miningParams: number[]) {
    return this.http.post<Unit[]>(`${environment.url}/admin/edit-mining-params`, miningParams);
  }

  updatePassword(passwords: string[]) {
    return this.http.post(`${environment.url}/admin/change-password`, passwords);
  }
}


