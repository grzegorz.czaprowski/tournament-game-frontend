import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../environments/environment';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BotListService {

  constructor(private http: HttpClient) {
  }

  public getBotsList(): Observable<string[]> {
    return this.http.get<string[]>(`${environment.tournamentSysUrl}/usrmgmt/user/bots`);
  }
}
