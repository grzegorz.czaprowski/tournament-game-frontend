import {Component, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {AdminPanelService} from '../admin-panel.service';
import {AdminLoginData} from '../model/AdminLoginData';
import {Observable} from 'rxjs';
import {Router} from '@angular/router';


@Component({
  selector: 'app-logging',
  templateUrl: './logging.component.html',
  styleUrls: ['./logging.component.css']
})
export class LoggingComponent implements OnInit {

  login: string;
  password: string;
  adminPanelService: AdminPanelService;
  adminLoginData: AdminLoginData;
  errorMsg: string;

  constructor(private http: HttpClient, private router: Router) {
  }

  ngOnInit() {
    this.adminPanelService = new AdminPanelService(this.http);
  }

  signIn() {
    this.adminLoginData = new AdminLoginData(this.login, this.password);
    this.adminPanelService.validateAdmin(this.adminLoginData).subscribe(res => {
      if (res != null) {
        this.router.navigate(['./admin-menu']);
      } else {
        this.errorMsg = 'Niepoprawny login lub hasło';
      }
    });
  }

}


