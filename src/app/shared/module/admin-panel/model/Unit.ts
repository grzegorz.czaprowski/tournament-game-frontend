export class Unit {
  name: string;
  hp: number;
  range: number;
  ap: number;
  dmg: number;
  cost: number;

  constructor(name: string, hp: number, range: number, ap: number, dmg: number, cost: number) {
    this.name = name;
    this.hp = hp;
    this.range = range;
    this.ap = ap;
    this.dmg = dmg;
    this.cost = cost;
  }
}
