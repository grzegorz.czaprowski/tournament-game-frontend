import {Component, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {AdminPanelService} from '../admin-panel.service';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.css']
})
export class ChangePasswordComponent implements OnInit {

  oldPassword: string;
  newPassword: string;
  newPasswordRepeated: string;
  passwordChangeResult: string;

  constructor(private http: HttpClient, private adminPanelService: AdminPanelService) {
  }

  ngOnInit() {
  }

  changePassword() {
    if (this.newPassword !== this.newPasswordRepeated) {
      this.passwordChangeResult = 'Hasła nie są identyczne';
      return;
    }
    this.adminPanelService.updatePassword([
      this.oldPassword,
      this.newPassword
    ]).subscribe(res => {
      if (res === 1) {
        this.passwordChangeResult = 'Hasło zostało zmienione';
      } else {
        this.passwordChangeResult = 'Hasło nie zostało zmienione';
      }
    });
  }

}
