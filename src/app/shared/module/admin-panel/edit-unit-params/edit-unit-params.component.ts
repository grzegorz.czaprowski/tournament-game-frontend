import {Component, OnInit} from '@angular/core';
import {AdminPanelService} from '../admin-panel.service';
import {HttpClient} from '@angular/common/http';
import {Unit} from '../model/Unit';

@Component({
  selector: 'app-edit-unit-params',
  templateUrl: './edit-unit-params.component.html',
  styleUrls: ['./edit-unit-params.component.css']
})
export class EditUnitParamsComponent implements OnInit {

  units: Unit[] = [];
  miningParams: number[] = []
  adminPanelService: AdminPanelService;

  constructor(private http: HttpClient) {
  }

  ngOnInit() {
    this.adminPanelService = new AdminPanelService(this.http);
    this.setUnitsList();
    this.setMiningParams();
  }

  saveChanges() {
    this.adminPanelService.editUnitParams(this.units).subscribe();
    this.adminPanelService.editMiningParams(this.miningParams).subscribe();
  }

  setUnitsList() {
    this.units = [];
    this.adminPanelService.getUnitParams().subscribe(res => {
      res.forEach(el => {
        this.units.push(new Unit(el['name'], el['hp'], el['range'], el['ap'], el['dmg'], el['cost']));
      });
    });
  }

  setMiningParams() {
    this.miningParams = [];
    this.adminPanelService.getMiningParams().subscribe(res => {
      this.miningParams[0] = res[0];
      this.miningParams[1] = res[1];
    });
  }

  reset() {
    this.setUnitsList();
    this.setMiningParams();
  }


}


