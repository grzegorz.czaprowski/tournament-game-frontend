import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {LoggingComponent} from './logging/logging.component';
import {ButtonModule} from 'primeng/button';
import {PasswordModule} from 'primeng/primeng';
import {FormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {AdminMenuComponent} from './admin-menu/admin-menu.component';
import {RouterModule} from '@angular/router';
import {EditUnitParamsComponent} from './edit-unit-params/edit-unit-params.component';
import {TableModule} from 'primeng/table';
import { ShowConnectedBotsComponent } from './show-connected-bots/show-connected-bots.component';
import { ChangePasswordComponent } from './change-password/change-password.component';

@NgModule({
  imports: [
    CommonModule,
    ButtonModule,
    PasswordModule,
    FormsModule,
    HttpClientModule,
    TableModule,
    RouterModule
  ],
  declarations: [
    LoggingComponent,
    AdminMenuComponent,
    EditUnitParamsComponent,
    ShowConnectedBotsComponent,
    ChangePasswordComponent
  ]
})
export class AdminPanelModule {
}
