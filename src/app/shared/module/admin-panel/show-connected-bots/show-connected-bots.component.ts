import {Component, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {BotListService} from '../bot-list.service';

@Component({
  selector: 'app-show-connected-bots',
  templateUrl: './show-connected-bots.component.html',
  styleUrls: ['./show-connected-bots.component.css']
})
export class ShowConnectedBotsComponent implements OnInit {

  botInfo: string[][] = [];

  constructor(private botListService: BotListService) {
  }

  ngOnInit() {
    this.getBotsListOnServer();
  }

  getBotsListOnServer() {
    this.botListService.getBotsList().subscribe(res => {
      res.forEach(el => {
        this.botInfo.push([el['name'], el['nick'], el['surname'], el['bot']]);
      });
    });
  }
}
