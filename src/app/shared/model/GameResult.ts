export interface GameResult {
  gameId: number;
  winnerId: number;
}
