export interface GameLog {
  date: string;
  content: string;
}
