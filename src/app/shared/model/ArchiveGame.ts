export class ArchiveGame {
  urlPlayer1: string;
  urlPlayer2: string;
  gameId: number;
  date: Date;

  constructor(urlPlayer1: string, urlPlayer2: string, gameId: number, date: Date) {
    this.urlPlayer1 = urlPlayer1;
    this.urlPlayer2 = urlPlayer2;
    this.gameId = gameId;
    this.date = date;
  }
}
