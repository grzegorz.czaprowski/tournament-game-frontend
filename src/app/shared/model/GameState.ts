export interface GameState {
  currentStep: number;
  currentTurn: number;
  gameId: number;
  lastActions: [
    {
      actionType: string;
      id: string;
      target: string;
    }
    ];
  lastLogs: string[];
  player1: {
    active: boolean,
    base: {
      coordinates: {
        x: number,
        y: number
      },
      hp: number,
      id: string,
      newUnit: {
        actionPoints: number,
        coordinates: {
          x: number,
          y: number
        },
        cost: number,
        damage: number,
        entrench: boolean,
        hp: number,
        id: string,
        name: string,
        owner: string,
        rangeOfAttack: 0
      },
      owner: string
    },
    gold: number,
    id: number,
    units: [
      {
        actionPoints: number,
        coordinates: {
          x: number,
          y: number
        },
        cost: number,
        damage: number,
        entrench: boolean,
        hp: number,
        id: string,
        name: string,
        owner: string,
        rangeOfAttack: number
      }
      ]
  };
  player2: {
    active: boolean,
    base: {
      coordinates: {
        x: number,
        y: number
      },
      hp: number,
      id: string,
      newUnit: {
        actionPoints: number,
        coordinates: {
          x: number,
          y: number
        },
        cost: number,
        damage: number,
        entrench: boolean,
        hp: number,
        id: string,
        name: string,
        owner: string,
        rangeOfAttack: number
      },
      owner: string
    },
    gold: number,
    id: number,
    units: [
      {
        actionPoints: number,
        coordinates: {
          x: number,
          y: number
        },
        cost: number,
        damage: number,
        entrench: boolean,
        hp: number,
        id: string,
        name: string,
        owner: string,
        rangeOfAttack: number
      }
      ]
  };
  mapPath: string;
  mines: [
    {
      coordinates: {
        x: number,
        y: number
      },
      id: string,
      owner: string,
      resourcesLeft: number,
      workersCount: number
    }
    ];
  tournamentId: number;
}
