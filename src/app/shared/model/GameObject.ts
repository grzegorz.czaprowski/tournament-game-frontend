export interface GameObject {
  'actionPoints': number;
  'coordinates': {
    'x': number;
    'y': number;
  };
  'cost': number;
  'damage': number;
  'entrench': boolean;
  'hp': number;
  'id': string;
  'owner': string;
  'rangeOfAttack': number;
}
