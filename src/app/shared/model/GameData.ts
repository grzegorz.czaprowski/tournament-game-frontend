export class GameData {

  gameID: number;
  player1ID: number;
  player1Url: string;
  player2ID: number;
  player2Url: string;
  responseUrl: string;
  tournamentID: number;
  winnerID: number;

  constructor() {
  }
}
