import { Injectable } from '@angular/core';
import { GameLog } from '../model/GameLog';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class GameLogService {

  constructor(private http: HttpClient) { }

  getGameLogs(): Observable<GameLog[]> {
    return this.http.get<GameLog[]>(
      `${environment.url}/gameLogs`);
  }
}
