import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class GameListService {

  constructor(private http: HttpClient) { }

  getActiveGames(): Observable<number[]> {
    return this.http.get<number[]>(
      `${environment.url}/getActiveGames`);
  }
}
