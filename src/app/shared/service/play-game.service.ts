import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../../environments/environment';
import {GameResult} from '../model/GameResult';

@Injectable({
  providedIn: 'root'
})
export class PlayGameService {

  constructor(private http: HttpClient) { }

  getResultGame(gameId: number): Observable<GameResult> {
    return this.http.get<GameResult>(
      `${environment.url}/getResultGame/${gameId}`
    );
  }
}
