import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {Observable} from 'rxjs';
import {ArchiveGame} from '../model/ArchiveGame';

@Injectable({
  providedIn: 'root'
})
export class GameLogsArchiveService {

  constructor(private http: HttpClient) { }

  getArchiveGamesList(): Observable<ArchiveGame[]> {
    return this.http.get<ArchiveGame[]>(
      `${environment.url}/getArchiveGames`
    );
  }
}
