import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import {environment} from '../../../environments/environment';
import {GameData} from '../model/GameData';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class StartGameService {

  constructor(private http: HttpClient) { }

  newGame(gameData: GameData) {
    return this.http.post(
      `${environment.url}/newGame`, gameData
    );
  }
}
