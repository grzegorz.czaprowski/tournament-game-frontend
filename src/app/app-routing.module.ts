import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {DashboardComponent} from './shared/module/dashboard/dashboard.component';
import {GameListComponent} from './shared/module/game-list/game-list.component';
import {GameLogsArchiveComponent} from './shared/module/game-logs-archive/game-logs-archive.component';
import {PlayGameComponent} from './shared/module/game-visualisation/play-game/play-game.component';
import {PlaybackComponent} from './shared/module/game-visualisation/playback/playback.component';
import {LoggingComponent} from './shared/module/admin-panel/logging/logging.component';
import {AdminMenuComponent} from './shared/module/admin-panel/admin-menu/admin-menu.component';
import {EditUnitParamsComponent} from './shared/module/admin-panel/edit-unit-params/edit-unit-params.component';
import {ShowConnectedBotsComponent} from './shared/module/admin-panel/show-connected-bots/show-connected-bots.component';
import {StartGameComponent} from './shared/module/start-game/start-game.component';
import {ChangePasswordComponent} from './shared/module/admin-panel/change-password/change-password.component';
import {CreditsComponent} from './shared/module/credits/credits.component';

const routes: Routes = [
  {path: '', redirectTo: '/dashboard', pathMatch: 'full'},
  {path: 'dashboard', component: DashboardComponent},
  {path: 'game-list', component: GameListComponent},
  {path: 'game-logs-archive', component: GameLogsArchiveComponent},
  {path: 'start-game', component: StartGameComponent},
  {path: 'play-game/:gameId', component: PlayGameComponent, pathMatch: 'full'},
  {path: 'play-game', component: PlayGameComponent},
  {path: 'playback', component: PlaybackComponent},
  {path: 'playback/:gameId', component: PlaybackComponent, pathMatch: 'full'},
  {path: 'admin-panel', component: LoggingComponent},
  {path: 'admin-menu', component: AdminMenuComponent},
  {path: 'edit-unit-params', component: EditUnitParamsComponent},
  {path: 'show-connected-bots', component: ShowConnectedBotsComponent},
  {path: 'change-password', component: ChangePasswordComponent},
  {path: 'credits', component: CreditsComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
